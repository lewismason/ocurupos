var async = require("async");
const jwt = require('jsonwebtoken');


var fetch = {
  CurrentUser: (UserID, resp) => {
    Users.model.findOne({_id: UserID}, function(err, result) {
      result = result.toJSON();
      delete result.__v;
      delete result.password;
      delete result.password2;
      return resp(result);
    });
  }
}

exports.fetch = fetch;
