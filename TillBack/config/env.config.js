module.exports = {
    "port": 3000,
    "appEndpoint": "http://localhost:3000",
    "apiEndpoint": "http://localhost:3000",
    "jwt_secret": "7CsMRA#.5ULP&pb",
    "jwt_expiration_in_seconds": 36000,
    "environment": "dev",
    "width": 1024,
    "height": 768,
};
