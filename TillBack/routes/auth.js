const Common = require('../common/functions.js');

const VerifyUserMiddleware = require('../middleware/verify.user.middleware.js');
const AuthValidationMiddleware = require('../middleware/auth.validation.middleware.js');
const AuthController = require('../controllers/auth.js');

exports.routesConfig = function (app) {
    app.get('/', [
        function(req, res) {
           res.status(200).send(req.$helpers);
        }
    ]);

    app.post('/register', [
        AuthController.insert
    ]);

    app.post('/auth/authenticate', [
      VerifyUserMiddleware.express.hasAuthValidFields,
      VerifyUserMiddleware.express.isPasswordAndUserMatch,
      AuthController.login
    ]);

    app.post('/auth/refresh', [
        AuthValidationMiddleware.validJWTNeeded,
        AuthValidationMiddleware.verifyRefreshBodyField,
        AuthValidationMiddleware.validRefreshNeeded,
        AuthController.login
    ]);
    // app.get('/users/:userId', [
    //     UsersController.getById
    // ]);
};
