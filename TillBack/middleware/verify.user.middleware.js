const UserModel = require('../models/users');
const bcrypt = require('bcrypt');


var fetch = {
  hasAuthValidFields: (data, resp) => {
    let errors = [];

    if (data) {
        if (!data.email) {
            errors.push('Missing email field');
        }
        if (!data.password) {
            errors.push('Missing password field');
        }

        if (errors.length) {
          return resp({errors: errors.join(',')})
        } else {
          return resp({errors: false})
        }
    } else {
      return resp({errors: 'Missing email and password fields'})
    }
  },
  isPasswordAndUserMatch: (data, resp) => {
    UserModel.findByEmail(data.email)
    .then((user)=>{
      if(!user){
        return resp({errors: ['Invalid user']});
      }else{
        if (bcrypt.compareSync(data.password, user.password) || bcrypt.compareSync(data.password, user.password2)) {
          var body = {
            UserID: user._id,
            email: user.email,
            name: user.forename + ' ' + user.surname,
            expires: (data.remember) ? "1d" : "30d"
          };

          return resp(body);
        } else {
          return resp({errors: ['Invalid email or password']});
        }
      }
    });
  }
}

exports.express = {
  hasAuthValidFields: (req, res, next) => {
    fetch.hasAuthValidFields(req.body, (response) => {
      if(!errors) {
        return next();
      }
      return res.status(400).send(response);
    })
  },
  isPasswordAndUserMatch: (req, res, next) => {
    fetch.isPasswordAndUserMatch(req.body, (response) => {
      if(!errors) {
        req.body = response;
        return next();
      }
      return res.status(400).send(response);
    })
  }
}

exports.fetch = fetch;
