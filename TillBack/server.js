const config = require('./config/env.config.js');
const mongoose = require('./config/database'); //database configuration
var jwt = require('jsonwebtoken');

const Common = require('./common/functions.js');

// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var socket = require('./socketio/server.js');
