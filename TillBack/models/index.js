module.exports = (helpers) => {
  const Users = require('../models/Users')(helpers);
  const TillCategories = require('../models/TillCategories')(helpers);
  const Products = require('../models/Products')(helpers);
  const Discounts = require('../models/Discounts')(helpers);
  const DiscountsGroups = require('../models/DiscountsGroup')(helpers);
  const TillLayout = require('../models/TillLayout')(helpers);
  const TillProducts = require('../models/TillProducts')(helpers);

  return {
    Users: Users,
    TillCategories: TillCategories,
    Products: Products,
    Discounts: Discounts,
    DiscountsGroups: DiscountsGroups,
    TillLayout: TillLayout,
    TillProducts: TillProducts
  }
}
