const mongoose = require('../config/database'); //database configuration
const bcrypt = require('bcrypt');
const saltRounds = 10;

//Define a schema
var ModelName = 'DiscountsGroup';
const ModelSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  items: {
    type: Array,
    required: true,
  },
  name: {
    type: String,
    required: true,
  }
},
{
  collection: ModelName,
  timestamps: true
});
// hash user password before saving into database
ModelSchema.pre('save', function(next){
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});


var Model;
if (mongoose.models[ModelName]) {
  Model = mongoose.model(ModelName);
} else {
  Model = mongoose.model(ModelName, ModelSchema);
}

var $helpers;
module.exports = (helpers = false) => {
  $helpers = helpers;

  return {
    m: Model,
  }
}
