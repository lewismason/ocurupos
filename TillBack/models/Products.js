const mongoose = require('../config/database'); //database configuration
const bcrypt = require('bcrypt');
const saltRounds = 10;

//Define a schema
var ModelName = 'Products';
const ModelSchema = new mongoose.Schema({
  tillCategory: {
    type: String,
    trim: true,
    required: false,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  value: {
    type: String,
    trim: true,
    required: false,
  },
  brand: {
    type: String,
    trim: true,
    required: false,
  },
  unit_of_measure: {
    type: Object,
    required: true,
  },
  abv: {
    type: String,
    trim: true,
    required: false,
  },
  units: {
    type: String,
    trim: true,
    required: false,
  },
  parentProduct: {
    type: String,
    trim: true,
    required: false,
  },
  Discounts: {
    type: Object
  },
  hasChild: {
    type: Boolean
  },
},
{
  collection: ModelName,
  timestamps: true
});
// hash user password before saving into database
ModelSchema.pre('save', function(next){
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});


var Model;
if (mongoose.models[ModelName]) {
  Model = mongoose.model(ModelName);
} else {
  Model = mongoose.model(ModelName, ModelSchema);
}

var $helpers;
module.exports = (helpers = false) => {
  $helpers = helpers;

  return {
    m: Model,
  }
}
