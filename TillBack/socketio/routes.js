module.exports = (socket) => {
  socket = require('./routes/auth.js')(socket);
  socket = require('./routes/test.js')(socket);
  socket = require('./routes/items.js')(socket);
  socket = require('./routes/cart.js')(socket);
  socket = require('./routes/layout.js')(socket);

  return socket;
}
