// const Business = require('../../models/business');
// const Employees = require('../../models/employees');

module.exports = (socket) => {
    const ItemsController = require('../controllers/items.js')(socket);

    // socket.on('company.employees', (data) => {CompanyController.socket.Employees_get(data, socket)});
    socket.on('items', ItemsController.get);
    socket.on('linkItems', ItemsController.linkItems);
    socket.on('Categories', ItemsController.getCategorys);
    socket.on('Products', ItemsController.getProducts);
    socket.on('getCategoryName', ItemsController.getCategoryName);
    socket.on('DiscountItems', ItemsController.DiscountItems);


    socket.on('links', ItemsController.links);


    return socket;
}
