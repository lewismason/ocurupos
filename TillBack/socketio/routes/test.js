// const Business = require('../../models/business');
// const Employees = require('../../models/employees');

const async = require('async');
module.exports = (socket) => {
    // const CartController = require('../controllers/cart.js')(socket.$helpers, socket);
    const Models = require('../../models/index.js')(socket.$helpers);

    socket.on('test', function(data){
      Models.TillLayout.m.find({isChild: { $ne: null }}, (err, Layouts) => {
        if(err){return callback(true);}

        async.forEachOf(Layouts, function(Layout, key, callback) {
            Models.TillProducts.m.find({_id: {$in: Layout.Products}}).exec(function(err, Products) {
              async.map(Products, (Item, mapCallback) => {
                console.log(Item)
                var Product = { ...Item._doc };

                var ReturnProduct = {
                  _id: Product._id,
                  stockProduct: Product.stockProduct,
                  name: Product.name,
                  pricing: Product.pricing,
                }

                return mapCallback(null, ReturnProduct);
              }, (err, ProductsArray) => {
                var Category = { ...Layout._doc };
                Category.isChild = (Category.isChild[0] != 'false') 
                Category.Products = ProductsArray;
                socket.emit('test', Category);
                return callback();
              });
            })
        }, function(err) {
            if( err ) {
              // console.log('Discount Doesnt Exist For those items')
            } else {
              // socket.emit('addToCartGroupDiscount', DiscountArray)
            }
        });


      });
    });

    return socket;
}
