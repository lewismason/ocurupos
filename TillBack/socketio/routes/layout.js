module.exports = (socket) => {
    const LayoutController = require('../controllers/layout.js')(socket);

    // socket.on('company.employees', (data) => {CompanyController.socket.Employees_get(data, socket)});
    socket.on('base_layout', LayoutController.base);
    socket.on('layout_category', LayoutController.categories);
    socket.on('layout_product', LayoutController.products);


    return socket;
}
