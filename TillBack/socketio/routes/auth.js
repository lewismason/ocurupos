// const Business = require('../../models/business');
// const Employees = require('../../models/employees');

module.exports = (socket) => {
    const AuthController = require('../controllers/auth.js')(socket);

    // socket.on('company.employees', (data) => {CompanyController.socket.Employees_get(data, socket)});
    socket.on('login', AuthController.login);

    return socket;
}
