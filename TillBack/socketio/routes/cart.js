// const Business = require('../../models/business');
// const Employees = require('../../models/employees');

module.exports = (socket) => {
    const CartController = require('../controllers/cart.js')(socket);

    // socket.on('company.employees', (data) => {CompanyController.socket.Employees_get(data, socket)});
    socket.on('addToCartGroupDiscount', CartController.addToCartGroupDiscount);
    socket.on('discountCheck', CartController.discountCheck);

    return socket;
}
