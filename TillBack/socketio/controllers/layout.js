const async = require('async');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

module.exports = (socket) => {
  const Models = require('../../models/index.js')(socket.$helpers);
  return {

    base: (data) => {
      Models.TillLayout.m.find({isChild: { $ne: null }}, (err, Layouts) => {
        if(err){return callback(true);}

        async.forEachOf(Layouts, function(Layout, key, callback) {
            Models.TillProducts.m.find({_id: {$in: Layout.Products}}).exec(function(err, Products) {
              async.map(Products, (Item, mapCallback) => {
                var Product = { ...Item._doc };

                var ReturnProduct = {
                  _id: Product._id,
                  stockProduct: Product.stockProduct,
                  name: Product.name,
                  pricing: Product.pricing,
                }

                return mapCallback(null, ReturnProduct);
              }, (err, ProductsArray) => {
                var Category = { ...Layout._doc };
                Category.isChild = (Category.isChild[0] != 'false')
                Category.Products = ProductsArray;


                socket.emit('layout_category', Category);
                return callback();
              });
            })
        }, function(err) {
            if( err ) {
              // console.log('Discount Doesnt Exist For those items')
            } else {
              // socket.emit('addToCartGroupDiscount', DiscountArray)
            }
        });
      });
    },
    categories: (data) => {
      var query = {
        isChild: data
      }

      if(data == 'base'){
        query = {
          isChild: 'false'
        }
      }

      Models.TillLayout.m.find(query, (err, Layouts) => {
        if(Layouts.length == 0)
        {
          return socket.emit('layout_category', false);
        }

        async.forEachOf(Layouts, function(LayoutItem, key, callback) {
          var Category = { ...LayoutItem._doc };

          socket.emit('layout_category', Category);
        }, function(err) {

        });
      });
    },
    products: (data) => {
      var query = {
        _id: {
          $in: data
        }
      }

      if(data == 'base'){
        query = {
          inCategory: false
        }
      }

      Models.TillProducts.m.find(query).exec(function(err, Products) {
        async.map(Products, (Item, mapCallback) => {
          var Product = { ...Item._doc };
          var ReturnProduct = {
            _id: Product._id,
            stockProduct: Product.stockProduct,
            name: Product.name,
            pricing: Product.pricing,
          }
          return mapCallback(null, ReturnProduct);
        }, (err, ProductsArray) => {
          if(ProductsArray.length > 0) {
            return socket.emit('layout_product', ProductsArray);
          }
        });
      })

    }

  }
}
