// const secret = require('../../config/env.config.js').jwt_secret;

// const UserModel = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

module.exports = (socket) => {
  const Models = require('../../models/index.js')(socket.$helpers);
  return {

    login: (data) => {
      Models.Users.m.findOne({userid: parseInt(data.user), passcode: parseInt(data.passcode)}, (err, result) => {
        if(err)
        {
          return socket.emit('login_failed', {
            errors: ['User ID or Passcode Incorrect']
          });
        }

        socket.userid = result._id;
        return socket.emit('login_success', result);
      });
    }

  }
}
