const async = require('async');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

module.exports = (socket) => {
  const Models = require('../../models/index.js')(socket.$helpers);
  return {

    addToCartGroupDiscount: (data) => {
      // console.log(data)

      Models.Discounts.m.findById(data.discountID).exec((err, DiscountArray) => {
        if(err) { return; }

        var itemGroups = DiscountArray.itemGroups

        async.forEachOf(data.items, function(item, key, callback) {
          if(itemGroups.includes(key))
          {
            Models.DiscountsGroups.m.findById(key).exec((err, DiscountGroupsArray) => {
              var items = DiscountGroupsArray.items;
              if(items.includes(item))
              {
                return callback();
              }else{
                return callback(true);
              }
            });
          }else{
            return callback(true);
          }
        }, function(err) {
            if( err ) {
              console.log('Discount Doesnt Exist For those items')
            } else {
              socket.emit('addToCartGroupDiscount', DiscountArray)
            }
        });
      });



      Models.Products.m.find({
        '_id': { $in: data}
      }).exec((err, response) => {
        if(err) { return; }


      });
    },
    discountCheck: (data) => {
      // var discountsArray = {};
      //
      // for (var i = 0; i < data.length; i++) {
      //   if(data[i] != null)
      //   {
      //     if(data[i].price.discounts)
      //     {
      //       for (var i1 = 0; i1 < data[i].price.discounts.length; i1++) {
      //         if(discountsArray[data[i].price.discounts[i1]] == undefined)
      //         {
      //           discountsArray[data[i].price.discounts[i1]] = {
      //             _id: data[i].price.discounts[i1],
      //             items: []
      //           };
      //         }
      //
      //         discountsArray[data[i].price.discounts[i1]].items.push([data[i]._id, data[i].cartIndex, data[i].price.value])
      //       }
      //     }
      //   }
      // }
      //
      // async.map(discountsArray, (item, mapCall) => {
      //
      //     var remainingArray = item.items.slice()
      //     var sortedItems = item.items.sort(function(obj1, obj2) {
      //     	return obj1[2] - obj2[2];
      //     });
      //     Models.Discounts.m.findById(item._id).exec((err, response) => {
      //       if(!err)
      //       {
      //         var groups = []
      //         if(response.type == 'link_save')
      //         {
      //           groups = response.groups;
      //         }
      //         if(response.type == 'multi_save')
      //         {
      //           for (var i = 0; i < response.multi_save; i++) {
      //             groups[i] = response.groups[0]
      //           }
      //         }
      //
      //           var DiscountedItems = [];
      //           if(item.items.length >= groups.length)
      //           {
      //             function addToDiscounted(array, groupLength, skipIndex = null)
      //             {
      //               if(array.length >= groupLength) {
      //                 // for (var i = 0; i < array.length; i++) {
      //
      //                 if(skipIndex != null)
      //                 {
      //                   var temparray = []
      //                   var usedIndexs = []
      //
      //                   for (var i = 0; i < array.length; i++) {
      //                     if(temparray.length < groupLength)
      //                     {
      //                       if(i != skipIndex) {
      //                         usedIndexs.push(i)
      //                         temparray.push(array[i])
      //                       }
      //                     }
      //                   }
      //
      //                 }else{
      //                   var temparray = []
      //                   var usedIndexs = []
      //                   for (var i = 0; i < array.length; i++) {
      //                     if(temparray.length < groupLength)
      //                     {
      //                       usedIndexs.push(i)
      //                       temparray.push(array[i])
      //                     }
      //                   }
      //                 }
      //
      //                 if(temparray.length == groupLength)
      //                 {
      //                   // console.log(temparray)
      //                   var itemTotal = 0;
      //                   for (var i = 0; i < temparray.length; i++) {
      //                     itemTotal += temparray[i][2]
      //                   }
      //
      //
      //                   var discountTotal = itemTotal - response.discount_price;
      //                   if(discountTotal > 0)
      //                   {
      //                     var newArray = array;
      //                     for (var i = 0; i < usedIndexs.length; i++) {
      //                       delete newArray[usedIndexs[i]]
      //                     }
      //
      //                     var filtered = array.filter(function (el) {
      //                       return el != undefined;
      //                     });
      //
      //                     var cartItemsUsed = [];
      //                     for (var i = 0; i < temparray.length; i++) {
      //                       cartItemsUsed.push(temparray[i][1]);
      //                     }
      //
      //                     DiscountedItems.push({
      //                       discountId: item._id,
      //                       discount: {
      //                         name: response.name,
      //                         type: response.type,
      //                         discount_price: response.discount_price,
      //                         discount_total: discountTotal
      //                       },
      //                       cartItemsUsed: cartItemsUsed
      //                     })
      //
      //                     // return newArray;
      //                     addToDiscounted(filtered, groupLength, null)
      //                   }else{
      //                     if(skipIndex != null)
      //                     {
      //                       skipIndex += 1
      //                     }else{
      //                       skipIndex = 0
      //                     }
      //
      //                     if(skipIndex < array.length)
      //                     {
      //                       if(array.length - 1 >= groupLength)
      //                       {
      //                         addToDiscounted(array, groupLength, skipIndex)
      //                       }
      //                     }
      //                   }
      //                 }
      //               }
      //             }
      //
      //             var DiscountStart = addToDiscounted(item.items, groups.length)
      //           }
      //           mapCall(false, DiscountedItems)
      //       }
      //     });
      // }, (err, DiscountsArray) => {
      //   if(!err)
      //   {
      //     var CartItemsUsed = [];
      //     var CartDiscounts = [];
      //
      //     for (var i = 0; i < DiscountsArray.length; i++) {
      //       var DiscountGroup = DiscountsArray[i]
      //       for (var i1 = 0; i1 < DiscountGroup.length; i1++) {
      //         var Discount = DiscountGroup[i1]
      //
      //         if(CartItemsUsed.length == 0)
      //         {
      //           CartItemsUsed.push(Discount.cartItemsUsed);
      //           CartDiscounts.push(Discount);
      //           console.log('Added Discount')
      //         }else{
      //           for (var x = 0; x < CartItemsUsed.length; x++) {
      //             var ifItemsExist = CartItemsUsed[x].some(r=> Discount.cartItemsUsed.includes(r))
      //
      //             if (ifItemsExist == true) {
      //               console.log(CartDiscounts[x].discountId)
      //
      //               if(Discount.discount.discount_total >= CartDiscounts[x].discount.discount_total)
      //               {
      //                 CartItemsUsed[x] = Discount.cartItemsUsed;
      //                 CartDiscounts[x] = Discount;
      //               }
      //             }else{
      //               CartItemsUsed.push(Discount.cartItemsUsed);
      //               CartDiscounts.push(Discount);
      //               console.log('Added Discount')
      //             }
      //           }
      //         }
      //
      //
      //       }
      //     }
      //
      //
      //     socket.emit('Discounts', CartDiscounts)
      //
      //   }
      // });
    }

  }
}
