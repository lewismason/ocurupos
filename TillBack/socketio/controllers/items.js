// const secret = require('../config/env.config.js').jwt_secret;
const async = require('async');
const map = require('async/map');
// const UserModel = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');


module.exports = (socket) => {
  const Models = require('../../models/index.js')(socket.$helpers);
  return {

    get: (data) => {
      var find = {
        type: {
          $ne: 'sub'
        }
      }
      if(data.parent != 'base') {
        find = {
          parentCategory: data.parent,
          type: 'sub'
        }
      }
      Models.TillCategories.m.find(find, (err, Categorys) => {
        var findParent = '##'
        if(!err)
        {
          findParent = {_id: data.parent};
        }
          Models.TillCategories.m.findOne(findParent, (err, ParentCategory) => {
              var findProducts = {tillCategory: null, parentProduct: null}
              if(data.parent != 'base') {
                findProducts = {tillCategory: data.parent, parentProduct: null}
              }

              Models.Products.m.find(findProducts).sort('order').exec(function(err, Products) {
                map(Products, (Item, callback) => {
                  var Product = Item;
                  Models.Products.m.find({
                    parentProduct: Product._id.toString()
                  }).sort('order').exec(function(err, SubProducts){
                    var ReturnProduct;
                    if(!err)
                    {
                      ReturnProduct = {
                        _id: Product._id,
                        tillCategory: Product.tillCategory,
                        name: Product.name,
                        brand: Product.brand,
                        unit_of_measure: Product.unit_of_measure,
                        abv: Product.abv,
                        units: Product.units,
                        pricing: SubProducts,
                        price_with: Products.price_with
                      }

                      if(Product.linkCategory){
                        ReturnProduct.linkCategory = Product.linkCategory
                      }
                      if(Product.linkProducts){
                        ReturnProduct.linkProducts = Product.linkProducts
                      }
                    }


                    return callback(null, ReturnProduct);
                  })
                }, (err, ProductsArray) => {
                  var returnData = {
                    Categorys: Categorys,
                    Products: ProductsArray,
                    ParentCategory: ParentCategory
                  };
                  return socket.emit('items', returnData);
                });
              })

          });

        return socket.emit('items_failed', {
          errors: ['Unknown Error']
        });
      });
    },
    linkItems: (data) => {
      Models.Products.m.find({
        '_id': {
          '$in': data
        }
      }).exec(function(err, Products) {
        map(Products, function(Item, callback) {
          var Product = Item;
          Models.Products.m.findOne({
            _id: Item.parentProduct
          }).exec(function(err, parentProduct){
            if(!err)
            {
              var ReturnProduct = {
                _id: parentProduct._id,
                name: parentProduct.name,
                brand: parentProduct.brand,
                unit_of_measure: parentProduct.unit_of_measure,
                abv: parentProduct.abv,
                units: parentProduct.units,
                price: {
                  _id: Item._id,
                  name: Item.name,
                  value: Item.value,
                  unit_of_measure: Item.unit_of_measure,
                },
                price_with: Item.price_with
              }
              return callback(null, ReturnProduct);
            }


          })
        }, function(err, ProductsArray){
          var returnData = ProductsArray;
          return socket.emit('linkItems', returnData);
        });
      })
    },
    getCategorys: (data) => {
      var find = {}
      find.type = {$ne: 'sub'}

      if(data != 'base') {
        find.type = 'sub';
        find.parentCategory = data.parent;
      }

      Models.TillCategories.m.find(find, (err, Categories) => {
        socket.emit('Categories', Categories)
      });
    },
    getProducts: (data) => {
      var findProducts = {tillCategory: null, parentProduct: null}
      if(data.parent != 'base' && !data.parentProduct) {
        findProducts.tillCategory = data.parent
      }
      if(data.parentProduct && data.type == 'sub') {
        findProducts = {parentProduct: data.parentProduct, hideFromParent: {$ne: true}}
      }

      Models.Products.m.find(findProducts).sort('order').exec(function(err, Products) {
        if(data.parentProduct && data.type == 'sub')
        {
          Models.Products.m.findById(data.parentProduct).exec(function(err, Parent){
            return socket.emit('ParentProduct', Parent);
          });
        }
        map(Products, (Item, callback) => {
          var Product = Item;
            var ReturnProduct;
            if(!err)
            {
              // console.log(Product.hasChild)
              var ReturnProduct = {
                _id: Product._id,
                tillCategory: Product.tillCategory,
                parentProduct: Product.parentProduct,
                hasChild: Product.hasChild,
                name: Product.name,
                brand: Product.brand,
                unit_of_measure: Product.unit_of_measure,
                abv: Product.abv,
                units: Product.units,
                // pricing: SubProducts,
                price_with: Products.price_with,
                Discounts: Product.Discounts,
                value: Product.value
              }

              return callback(null, ReturnProduct);
            }


          // })
        }, (err, ProductsArray) => {
          // var returnData = {
          //   Categorys: Categorys,
          //   Products: ProductsArray,
          //   ParentCategory: ParentCategory
          // };
          return socket.emit('Products', ProductsArray);
        });
      })
    },
    getCategoryName: (data) => {
      Models.TillLayout.m.findById(data, (err, Category) => {
        socket.emit('CategoryName', Category)
      });
    },
    DiscountItems: (data) => {
      Models.Discounts.m.findById(data.Discounts[0].discountID).exec((err, DiscountArray) => {
        if(err) { return; }

        var itemGroups = DiscountArray.itemGroups

        if(itemGroups.includes(data.Discounts[0].itemGroup))
        {
          // var DiscountGroups = delete itemGroups[itemGroups.indexOf(data.Discounts[0].itemGroup)]
          itemGroups.splice(itemGroups.indexOf(data.Discounts[0].itemGroup), 1 )
          // socket.emit('DiscountItems', itemGroups)
          // console.log(itemGroups)
          Models.DiscountsGroups.m.find({
            _id: {
              $in: itemGroups
            }
          }).exec((err, DiscountGroupsArray) => {
            async.forEachOf(DiscountGroupsArray, function(item, key, callback) {
              Models.Products.m.find({
                _id: {
                  $in: item.items
                }
              }).exec((err, ProductArray) => {
                async.map(ProductArray, (Item, mapCall) => {
                  if(Item.parentProduct != undefined)
                  {
                    Models.Products.m.findById(Item.parentProduct).exec(function(err, response){
                      var returnItem = JSON.parse(JSON.stringify(Item))
                      returnItem.Parent = response
                      mapCall(null, returnItem)
                    })
                  }else{
                    mapCall(null, Item)
                  }
                }, (err, ProductsArray) => {
                  socket.emit('DiscountItems', {
                    groupId: item._id,
                    groupName: item.name,
                    groupLength: itemGroups.length + 1,
                    products: ProductsArray,
                  });
                  callback()
                });

              });
            }, function(err) {
                if( err ) {
                  console.log('Discount Doesnt Exist For those items')
                } else {
                  // console.log(DiscountArray.name)
                }
            });
          });
        }

        // async.forEachOf(data.items, function(item, key, callback) {
        //   if(itemGroups.includes(key))
        //   {
        //     Models.DiscountsGroups.m.findById(key).exec((err, DiscountGroupsArray) => {
        //       var items = DiscountGroupsArray.items;
        //       if(items.includes(item))
        //       {
        //         return callback();
        //       }else{
        //         return callback(true);
        //       }
        //     });
        //   }else{
        //     return callback(true);
        //   }
        // }, function(err) {
        //     if( err ) {
        //       console.log('Discount Doesnt Exist For those items')
        //     } else {
        //       console.log(DiscountArray.name)
        //     }
        // });

      });
    },
    links: (data) => {
      async.forEachOf(data, function(linkGroup, linkGroupKey, linkGroupCallback) {
        if(Array.isArray(linkGroup))
        {
          async.forEachOf(linkGroup, function(SubLinkGroup, SubLinkGroupKey, SubLinkGroupCallback) {
            async.map(SubLinkGroup.items, function(linkProduct, callback) {
              var splitter = linkProduct.toString().split("|");
              var productID = splitter[0];
              var productPrice = splitter[1];

              Models.TillProducts.m.findOne({
                _id: productID
              }).exec(function(err, product){
                if(!err)
                {
                  var ReturnProduct = {
                    _id: product._id,
                    name: product.name,
                    stockProduct: product.stockProduct,
                    price: product.pricing[productPrice],
                    priceIndex: productPrice,
                    extra: parseFloat(splitter[2])
                  }

                  return callback(null, ReturnProduct);
                }
              })
            }, function(err, ProductsArray){
              socket.emit('linkGroup', {
                linkGroup: [linkGroupKey, SubLinkGroupKey],
                linkItems: ProductsArray
              });
              SubLinkGroupCallback();
              return;
            });
          });
        }else{
          async.map(linkGroup.items, function(linkProduct, callback) {
            var splitter = linkProduct.toString().split("|");
            var productID = splitter[0];
            var productPrice = splitter[1];

            Models.TillProducts.m.findOne({
              _id: productID
            }).exec(function(err, product){
              if(!err)
              {
                var ReturnProduct = {
                  _id: product._id,
                  name: product.name,
                  stockProduct: product.stockProduct,
                  price: product.pricing[productPrice],
                  priceIndex: productPrice,
                  extra: parseFloat(splitter[2])
                }
                return callback(null, ReturnProduct);
              }
            })
          }, function(err, ProductsArray){
            socket.emit('linkGroup', {
              linkGroup: linkGroupKey,
              linkItems: ProductsArray
            });
            linkGroupCallback();
            return;
          });
        }
      }, function(err) {

      });
    }
  }
}
