var secret = require('../config/env.config.js').jwt_secret;
var io            = require("socket.io").listen(8121);


// io2.on('connection', function (socket) {
//   const AuthController = require('../controllers/auth.js')(false);
//   const VerifyUserMiddleware = require('../middleware/verify.user.middleware.js');
//   const AuthValidationMiddleware = require('../middleware/auth.validation.middleware.js');
//   socket.on('login', (data) => {
//     var fetch = VerifyUserMiddleware.fetch;
//     fetch.hasAuthValidFields(data, (response) => {
//       if(response.errors == false) {
//         fetch.isPasswordAndUserMatch(data, (tokenBody) => {
//           console.log(tokenBody)
//           if(!tokenBody.errors) {
//             return AuthController.socket.login(tokenBody, socket)
//           }
//           return socket.emit('login_failed', response);
//         })
//       }else{
//         return socket.emit('login_failed', response);
//       }
//     });
//   });
// });

io.on('connect', function (socket) {
  socket.emit('connected', {});
  socket.on('authorise', function(data) {
    socket.userid = data.user
  });
});

io.on('connection', function (socket) {
  if(!socket.$helpers){socket.$helpers = {};}

  // socket._id = socket.decoded_token.UserID;

  socket = require('./middleware.js')(socket);
  socket = require('./routes.js')(socket);

});
