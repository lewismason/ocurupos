module.exports = {
  "mongodb_ip": "62.30.206.226",
  "socket_ip": "http://62.30.206.226:8121",
  "screenType": "768p",
  "screenSize": {
    "1080p": {
      "width": 1920,
      "height": 1080,
    },
    "768p": {
      "width": 1024,
      "height": 768,
    }
  }

}
