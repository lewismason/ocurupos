const mongoose = require('../config/database'); //database configuration
const bcrypt = require('bcrypt');

const LocationSchema = new mongoose.Schema({
  Business_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
   },
   identifier: {
     type: String,
     trim: true,
     required: true
   }
},
{
  collection: 'locations',
  timestamps: true
});
// hash user password before saving into database
LocationSchema.pre('save', function(next){
    next();
});

var Locations;
if (mongoose.models.Locations) {
  Locations = mongoose.model('Locations');
} else {
  Locations = mongoose.model('Locations', LocationSchema);
}

// exports.createUser = (userData) => {
//     userData.settings = {
//       notifications: {
//         login: ['email']
//       }
//     }
//     userData.metadata = {
//       phone_number: null
//     }
//     const user = new User(userData);
//     return user.save();
// };
//
// exports.findByEmail = (email) => {
//     return User.find({email: email});
// };
//
var $helpers;
module.exports = (helpers = false) => {
  $helpers = helpers;

  return {
    model: Locations
  }
}
// exports.findById = (id) => {
//     return Locations.findById(id)
//         .then((result) => {
//             result = result.toJSON();
//             delete result.__v;
//             return result;
//         });
// };
// exports.model = Locations;
