import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
var Config = require('./env.config.js');

import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter,
{
  symbol : '£',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: false
})
import Dayjs from 'vue-dayjs';
Vue.use(Dayjs, {
  lang: 'en',
  filters: {},
  directives: {}
});


import IdleVue from 'idle-vue'
const eventsHub = new Vue()
// Vue.use(IdleVue, {
//   eventEmitter: eventsHub,
//   idleTime: 23000
// })

import io from 'socket.io-client';

var user = 'null';
if(localStorage.getItem('user'))
{
  user = JSON.parse(localStorage.getItem('user'))._id
}

const socket = io(Config.socket_ip, {});
socket.on('connected', function(resp) {
  if(localStorage.getItem('user'))
  {
    socket.emit('authorise', {user: JSON.parse(localStorage.getItem('user'))._id});
  }
});

Vue.prototype.$socket = socket;
Vue.prototype.$user = (localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : false;
Vue.use(Vuetify, {
  iconfont: 'mdi'
})
Vue.config.productionTip = false;


Vue.mixin({
  computed: {
    pageHeight: function(){
      // return Config.screenSize[Config.screenType].height;
      return window.innerHeight
    },
    pageWidth: function(){
      // return Config.screenSize[Config.screenType].width;
      return window.innerWidth
    },
    height24: function(){
        return this.pageHeight - 24;
    },
    height75: function(){
        return this.pageHeight - 75;
    },
    height99: function(){
        return this.pageHeight - 99;
    },
    heightCart: function(){
        return this.pageHeight - 250;
    }
  }
})

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
	if(!requiresAuth && localStorage.getItem('user'))
	{
		next({name: 'dash'});
	}
	if (requiresAuth && !localStorage.getItem('user')) {
		next({name: 'login'});
	} else {
		// if(store.getters.isAuthenticated)
		// {
		// 	helpers = require('./Vue/Helpers.js')(socket);
		// }
		next();
	}
});

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title + ' - EzPOS';
    next()
});
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
